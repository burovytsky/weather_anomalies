({
    doInit : function(component, event, helper) {
        component.set("v.isLoading", true);
        helper.getAnomalies(component, event, helper);
    },
    
    handleListSelect : function(component, event, helper){
        var selectedMenuItemValue = event.getParam("value");
                console.log('handsel' + selectedMenuItemValue);
        if(selectedMenuItemValue !== component.get("v.currentListViewName")){
            component.set("v.currentListViewName", selectedMenuItemValue);
            helper.handleRecordVisibility(component, event, helper, selectedMenuItemValue);
        }
    },
    
    onMenuOpen : function(component, event, helper) {
        var selectedOption = component.get("v.currentListViewName");
        console.log('onmenuopen' + selectedOption);
        for(var item of component.find("menuItems")){
            item.set("v.checked", item.get("v.value") === selectedOption);
        }
    },
    
    getWeather : function(component, event, helper){
        component.set("v.isLoading", true);
        let action = component.get("c.getWeatherNow");
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                helper.getAnomalies(component, event, helper);
            }else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    generateReport: function(component, event, helper) {
        component.set("v.isLoading", true);
        var action = component.get("c.generateAnomalyReport");
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Report was generated!",
                    "type": "success"
                });
                toastEvent.fire();
                helper.getAnomalies(component, event, helper);
            } else{
                alert('Something went wrong');
            }
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    }
})
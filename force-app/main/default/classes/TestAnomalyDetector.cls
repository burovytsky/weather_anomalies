@isTest
private class TestAnomalyDetector {

	@testSetup static void setup(){
		City__c city = new City__c(Name ='TestCity', City_Code__c = '123', Country__c = 'CA');
		Anomaly_Range_Configuration__c anomalyRangeConfiguration = new Anomaly_Range_Configuration__c(Max_Humidity__c = 105,
		                                                                                              Min_Humidity__c = 100,
		                                                                                              Min_Pressure__c = 900,
		                                                                                              Max_Pressure__c = 1000,
		                                                                                              Max_Temperature__c = 300,
		                                                                                              Min_Temperature__c = 250);
		insert anomalyRangeConfiguration;
		insert city;
	}
	@isTest static void createHumidityAnomalyTest(){
		List<Weather__c> testWeatherList = TestDataFactory.createWeathers(1,110,950,280,270);
		Test.startTest();
		insert testWeatherList;
		Test.stopTest();
		List<Anomaly__c> anomalies = [select city_name__c, type__c, weather__c from Anomaly__c];
		System.assert (anomalies.size() == 1);
		System.assertEquals('Humidity Anomaly', anomalies.get(0).Type__c);
	}
	@isTest static void createPressureAnomalyTest(){
		List<Weather__c> testWeatherList = TestDataFactory.createWeathers(1,104,1200,280,270);
		Test.startTest();
		insert testWeatherList;
		Test.stopTest();
		List<Anomaly__c> anomalies = [select city_name__c, type__c, weather__c from Anomaly__c];
		System.assert (anomalies.size() == 1);
		System.assertEquals('Pressure Anomaly', anomalies.get(0).Type__c);
	}
	@isTest static void createTemperatureAnomalyTest(){
		List<Weather__c> testWeatherList = TestDataFactory.createWeathers(1,104,999,400,280);
		Test.startTest();
		insert testWeatherList;
		Test.stopTest();
		List<Anomaly__c> anomalies = [select city_name__c, type__c, weather__c from Anomaly__c];
		System.assert (anomalies.size() == 1);
		System.assertEquals('Temperature Anomaly', anomalies.get(0).Type__c);
	}
	@isTest static void dontCreateAnomalyTest(){
		List<Weather__c> testWeatherList = TestDataFactory.createWeathers(1,104,950,280,270);
		Test.startTest();
		insert testWeatherList;
		Test.stopTest();
		List<Anomaly__c> anomalies = [select city_name__c, type__c, weather__c from Anomaly__c];
		System.assert (anomalies.isEmpty());
	}
	@isTest static void create200AnomaliesTest(){
		List<Weather__c> testWeatherList = TestDataFactory.createWeathers(200,120,950,280,270);
		Test.startTest();
		insert testWeatherList;
		Test.stopTest();
		List<Anomaly__c> anomalies = [select city_name__c, type__c, weather__c from Anomaly__c];
		System.assert (anomalies.size() == 200);
	}

	@isTest static void createThreeAnomaliesFromOneWeatherTest(){
		List<Weather__c> testWeatherList = TestDataFactory.createWeathers(1,120,1200,400,270);
		Test.startTest();
		insert testWeatherList;
		Test.stopTest();
		List<Anomaly__c> anomalies = [select city_name__c, type__c, weather__c from Anomaly__c];
		System.assert (anomalies.size() == 3);
	}
}
@isTest
public class OpenWeatherCalloutServiceTest {

	@testSetup static void setup(){
		City__c city = new City__c(Name ='TestCity', City_Code__c = '123', Country__c = 'CA');
		insert city;
	}
	@isTest static void getWeatherFromOpenWeatherWithStaticResourceTest(){
		Test.startTest();
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('getWeatherResource');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
		Test.setMock(HttpCalloutMock.class, mock);
		OpenWeatherCalloutService service = new OpenWeatherCalloutService();
		service.getWeatherFromOpenWeather();
		List<Weather__c> weatherList = [select id, Temperature__c, Visibility__c, Pressure__c from weather__c];
		System.assertEquals(1, weatherList.size());
		System.assertEquals(282.55, weatherList.get(0).Temperature__c);
		System.assertEquals(16093, weatherList.get(0).Visibility__c);
		Test.stopTest();
	}
	@isTest static void getWeatherFromOpenWeatherWithoutCloudsAndWindSpeedTest(){
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new OpenWeatherCalloutMock());
		OpenWeatherCalloutService service = new OpenWeatherCalloutService();
		service.getWeatherFromOpenWeather();
		List<Weather__c> weatherList = [select id, Temperature__c, Visibility__c, Pressure__c, Wind_Speed__c, Cloudiness__c from weather__c];
		System.debug(weatherList.get(0));
		System.assertEquals(1, weatherList.size());
		System.assertEquals(282.55, weatherList.get(0).Temperature__c);
		System.assertEquals(16093, weatherList.get(0).Visibility__c);
		System.assertEquals(null, weatherList.get(0).Wind_Speed__c);
		System.assertEquals(null, weatherList.get(0).Cloudiness__c);
		Test.stopTest();
	}

	@isTest static void whenStatusCode400DontCreateWeather(){
		Test.startTest();
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('getWeatherResource');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
		Test.setMock(HttpCalloutMock.class, mock);
		OpenWeatherCalloutService service = new OpenWeatherCalloutService();
		service.getWeatherFromOpenWeather();
		List<Weather__c> weatherList = [select id, Temperature__c, Visibility__c, Pressure__c from weather__c];
		System.assertEquals(0, weatherList.size());
		Test.stopTest();
	}
}
global class ScheduledJob implements Schedulable {

	private static final String CORN_EXP = '0 0 8 1/1 * ? *';

	global void execute(SchedulableContext ctx){
		ScheduledJob.getWeathers();
	}

	public static void runJob(){
		System.schedule('Weather job', CORN_EXP, new ScheduledJob());
	}
    @future(callout=true)
    public static void getWeathers(){
        OpenWeatherCalloutService owcs = new OpenWeatherCalloutService();
		owcs.getWeatherFromOpenWeather();
    }
}
public with sharing class AnomalyViewComponentController {

	@AuraEnabled
	public static List<Anomaly__c> getAnomalies(){
		return [SELECT Id, Name, Type__c, City__c, Weather__c, City__r.Name, Weather__r.Name, Reported__c, CreatedDate
		        FROM Anomaly__c ORDER BY CreatedDate DESC, Name DESC];
	}

	@AuraEnabled
	public static void getWeatherNow(){
		new OpenWeatherCalloutService().getWeatherFromOpenWeather();
	}

	@AuraEnabled
	public static void generateAnomalyReport(){
		Anomaly_Report__c report = new Anomaly_Report__c(Name = 'Anomaly Report ' + System.now().format('yyyy.MM.dd h:mm a'));
		insert report;
		List<Anomaly__c> notReportedAnomalies = [SELECT Reported__c FROM Anomaly__c where Reported__c = false];
		for(Anomaly__c anomaly : notReportedAnomalies){
			anomaly.reported__c = true;
		}
		update notReportedAnomalies;
		ContentVersion cv = new ContentVersion();
		cv.Title = report.Name;
		cv.PathOnClient = cv.Title + '.pdf';
		cv.IsMajorVersion = true;
		cv.FirstPublishLocationId = report.Id;
		cv.VersionData = Page.AnomalyReportPage.getcontentAsPdf();
		insert cv;
	}
}
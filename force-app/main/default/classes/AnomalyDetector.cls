public with sharing class AnomalyDetector {
	public Anomaly_Range_Configuration__c anomalyRandeSetting;

	public AnomalyDetector(){
		this.anomalyRandeSetting = Anomaly_Range_Configuration__c.getOrgDefaults();
	}

	public List<Anomaly__c> detectAnomaly(List<Weather__c> weathers){
		List<Anomaly__c> anomaliesToSave = new List<Anomaly__c> ();
		Set<Id> weatherIdSet = new Set<Id> ();
		for(Weather__c w : weathers){
			weatherIdSet.add(w.id);
		}
		weathers = [select id,city__c, city__r.name, humidity__c, pressure__c, Minimal_Temperature__c, Maximum_Temperature__c
		            from Weather__c where id in: weatherIdSet];
		for(Weather__c weather : weathers){
			if(weather.Humidity__c < anomalyRandeSetting.Min_Humidity__c ||
			   weather.Humidity__c > anomalyRandeSetting.Max_Humidity__c){
				anomaliesToSave.add(createAnomaly(weather, 'Humidity Anomaly'));
			}
			if(weather.Pressure__c < anomalyRandeSetting.Min_Pressure__c ||
			   weather.Pressure__c > anomalyRandeSetting.Max_Pressure__c){
				anomaliesToSave.add(createAnomaly(weather, 'Pressure Anomaly'));
			}
			if(weather.Minimal_Temperature__c < anomalyRandeSetting.Min_Temperature__c ||
			   weather.Maximum_Temperature__c > anomalyRandeSetting.Max_Temperature__c){
				anomaliesToSave.add(createAnomaly(weather, 'Temperature Anomaly'));
			}
		}
		return anomaliesToSave;
	}
	private Anomaly__c createAnomaly(Weather__c weather, String type){
		return new Anomaly__c( City__c= weather.City__c,
		                       City_Name__c = weather.City__r.name,
		                       Reported__c = false,
		                       Type__c = type,
		                       Weather__c = weather.Id);
	}
}
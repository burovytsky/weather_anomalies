public with sharing class OpenWeatherCalloutService {
	private static final String ENDPOINT_URL = 'https://api.openweathermap.org/data/2.5/weather';
	private static final String OPEN_WEATHER_API_KEY = Anomaly_Range_Configuration__c.getOrgDefaults().OPEN_WEATHER_API_KEY__c;
	private Map <String, City__c> cityCodeToCity;

	public OpenWeatherCalloutService(){
		this.cityCodeToCity = new Map<String, City__c> ();
		for(City__c c : [select id, city_code__c from City__c]){
			cityCodeToCity.put(c.City_Code__c, c);
		}
	}

	public void getWeatherFromOpenWeather(){
		List<Weather__c> weatherList = new List<Weather__c> ();
		for(City__c city : cityCodeToCity.values()){
			Weather__c weather = makeCallout(city.City_Code__c);
            System.debug('getWeatherFromOpenWeather ' +weather);
			if(weather != null){
				weatherList.add(weather);
			}
		}
		insert weatherList;
	}
	public Weather__c makeCallout(String cityCode){
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(ENDPOINT_URL + '?id=' + cityCode + '&appid=' + OPEN_WEATHER_API_KEY);
		request.setMethod('GET');
        request.setTimeout(120000);
		HttpResponse response = http.send(request);
		if(response.getStatusCode() == 200){
			OpenWeatherResponse owResponse = (OpenWeatherResponse) JSON.deserialize(response.getBody(), OpenWeatherResponse.class);
            return generateWeather(owResponse, cityCode);
		} else {
			System.debug('Status code: ' + response.getStatusCode());
			return null;
		}
	}
	public Weather__c generateWeather(OpenWeatherResponse response, String cityCode){
		Weather__c weather = new Weather__c(
			City__c = cityCodeToCity.get(cityCode).id,
			Temperature__c = response.Main.temp,
			Temperature_Feels_Like__c = response.Main.feels_like,
			Minimal_Temperature__c = response.Main.temp_min,
			Maximum_Temperature__c = response.Main.temp_max,
			Pressure__c = response.Main.pressure,
			Humidity__c = response.Main.humidity,
			Visibility__c = response.visibility,
			Cloudiness__c = response.Clouds.all);
		weather.Wind_Speed__c = response.Wind == null ? null : response.Wind.speed;
		weather.Cloudiness__c = response.Clouds == null ? null : response.Clouds.all;
		return weather;
	}

	public class OpenWeatherResponse {
		public Main main;
		public Wind wind;
		public Clouds clouds;
		public Integer visibility;
	}
	public class Main {
		public Decimal temp;
		public Decimal feels_like;
		public Decimal temp_max;
		public Decimal temp_min;
		public Integer pressure;
		public Integer humidity;
	}
	public class Wind {
		public Decimal speed;
	}
	public class Clouds {
		public Integer all;
	}
}
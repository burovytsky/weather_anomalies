@isTest
global class OpenWeatherCalloutMock implements HttpCalloutMock {
	global HTTPResponse respond(HTTPRequest request){
		HttpResponse response = new HttpResponse();
		response.setHeader('Content-Type', 'application/json');
		response.setBody('{"main": {"temp": 282.55, "feels_like": 281.86,"temp_min": 280.37,"temp_max": 284.26, "pressure": 1023,"humidity": 100},"visibility": 16093, "wind": {"speed": null, "deg": null},"clouds": {"all": null}}');
		response.setStatusCode(200);
		return response;
	}
}
@isTest
public class TestDataFactory {
	public static List<Weather__c> createWeathers(Integer numWeathers,
	                                              Integer humidity,
	                                              Integer pressure,
	                                              Integer maxTemperature,
	                                              Integer minTemperature){


		List<Weather__c> testWeatherList = new List<Weather__c> ();
		String cityId = [select id,name from City__c where name = 'TestCity'].get(0).id;
		for(Integer i = 0; i < numWeathers; i++){
			testWeatherList.add(new Weather__c(city__c = cityId,
			                                   humidity__c = humidity,
			                                   Minimal_Temperature__c = minTemperature,
			                                   Maximum_Temperature__c = maxTemperature,
			                                   pressure__c = pressure
			                                   ));
		}

		return testWeatherList;
	}
}
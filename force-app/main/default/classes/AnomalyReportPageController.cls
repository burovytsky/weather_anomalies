public with sharing class AnomalyReportPageController {
	public PageWrapper resultWrapper {get; set;}
	public AnomalyReportPageController(){
		List<AggregateResult> resultList = [SELECT COUNT(id) anomalyCount, City__r.Name cityName, Type__c anomalyType
		                                    FROM Anomaly__c
		                                    where Reported__c = false
		                                                        GROUP BY City__r.Name, Type__c];
		Map<String, Row> rowMap = new Map<String, Row> ();
		Integer count = 0;
		for(AggregateResult res : resultList){
			String cityName = String.valueOf(res.get('cityName'));
			String anomalyType  = String.valueOf(res.get('anomalyType'));
			count += Integer.valueOf(res.get('anomalyCount'));
			if(!rowMap.containsKey(cityName)){
				Row anomalyRow = new Row(cityName, '0','0','0');
				rowMap.put(cityName, anomalyRow);
			}

			if(anomalyType == 'Temperature Anomaly'){
				rowMap.get(cityName).temperatureAnomaly = String.valueOf(res.get('anomalyCount'));
			} else if(anomalyType == 'Pressure Anomaly'){
				rowMap.get(cityName).pressureAnomaly = String.valueOf(res.get('anomalyCount'));
			} else if(anomalyType == 'Humidity Anomaly'){
				rowMap.get(cityName).humidityAnomaly = String.valueOf(res.get('anomalyCount'));
			}
		}
		String startDate = formatDate((DateTime)[select min(createdDate) startDate from anomaly__c][0].get('startDate'));
		String endDate = formatDate((DateTime)[select max(createdDate) endDate from anomaly__c][0].get('endDate'));
		String currentDate = formatDate(System.now());

		String anomalyCount = String.valueOf(count);
		this.resultWrapper = new PageWrapper(startDate, endDate, currentDate, rowMap.values(), anomalyCount);
	}

	private static String formatDate(DateTime weatherTime){
		return weatherTime.format('yyyy.MM.dd hh:mm aaa');
	}
	public class PageWrapper {
		public String startDate {get; set;}
		public String endDate {get; set;}
		public String currentDate {get; set;}
		public List<Row> rowList {get; set;}
		public String anomalyCount {get; set;}

		public PageWrapper(String startDate, String endDate, String currentDate, List<Row> rowList, String anomalyCount){
			this.startDate=startDate;
			this.endDate = endDate;
			this.currentDate = currentDate;
			this.rowList =rowList;
			this.anomalyCount = anomalyCount;
		}
	}

	public class Row {
		public String cityName {get; set;}
		public String temperatureAnomaly {get; set;}
		public String pressureAnomaly {get; set;}
		public String humidityAnomaly {get; set;}

		public Row(String cityName, String temperatureAnomaly, String pressureAnomaly, String humidityAnomaly){
			this.cityName = cityName;
			this.temperatureAnomaly = temperatureAnomaly;
			this.pressureAnomaly = pressureAnomaly;
			this.humidityAnomaly = humidityAnomaly;
		}
	}
}
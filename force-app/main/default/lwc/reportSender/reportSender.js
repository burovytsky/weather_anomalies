import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getUsers from '@salesforce/apex/SendReportController.getUsers';
import sendReport from '@salesforce/apex/SendReportController.sendReport';

export default class ReportSender extends LightningElement {
    @api recordId;
    selected = [];
    options = [];
    isDisabled = true;

    connectedCallback() {
        getUsers().then(result => {
            this.options = [];
            result.forEach(result => {
                this.options.push({ 'label': result.Email, 'value': result.Id });
            });
        })
            .catch((error) => {
                console.log('error: ' + error);
            });
    }

    get selected() {
        return this.selected.length ? this.selected : 'none';
    }

    handleChange(event) {
        this.selected = event.detail.value;
        this.selected.length > 0 ? this.isDisabled = false : this.isDisabled = true;
    }

    handleSend() {
        sendReport({ anomalyReportId: this.recordId, selectedUsers: this.selected })
            .then((result) => {
                console.log('result send report' + JSON.stringify(result));
                    const event = new ShowToastEvent({
                        title: result ? 'Error' : 'Success',
                        message: result ? 'Something was wrong' : 'Email was successfully sent',
                        variant: result ? 'error' : 'success',
                        mode: 'dismissable'
                    });
                    this.dispatchEvent(event);
                
            })
            .catch((error) => {
                console.log('error: ' + error);
            });
    }

}